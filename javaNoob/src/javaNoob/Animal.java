package javaNoob;

public abstract class Animal {
	
	
	private String name;
	private int num;
	
	
	
	//public animal() {
		//System.out.println("No arguments"); //no arg constructor
	//}
	//public animal(String name, int num) {
	//	System.out.println(name +" "+ num);  //arg constructor 
	//}
	
	public Animal(String name ,int num ) {
		this.name = name;						//this field 
		this.num = num;
	
	
		
		
	}
	
	
	public String getName() {
		return name;
	}
	
	public int getnum() {
		return num;
		
	}
	
	
	public void setName(String name) {
		this.name=name;
		
	}
	
	public void setNum(int num) {
		this.num=num;
	}
}
