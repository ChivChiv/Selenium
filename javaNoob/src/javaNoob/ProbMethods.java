package javaNoob;

public class ProbMethods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] x = {1,2,7,15,24,12,14,13,4,2,23};
		System.out.println("The sum of even numbers is " + evenSum(x));
		System.out.println("The sum of odd numbers is " + oddSum(x));
		System.out.println("The min value is " +findMin(x));
		System.out.println("The min value is " +findMax(x));

		
	}
	
	
	public static int evenSum(int []x) {
		int sum=0;
		for(int i=0;i<x.length;i++) {
			if(x[i]%2==0)
				sum = sum + x[i];
		
	}
		return sum;
		
	}
	
	public static int oddSum(int []x) {
		int sum=0;
		for(int i=0;i<x.length;i++) {
			if(x[i]%2==1)
				sum = sum + x[i];
		
	}
		return sum;
		
	}
	
	public static int findMin(int[] x) {
		int min=x[0];
		for(int i=0; i<x.length;i++) {
			if(x[i]<min)
				min=x[i];
		}
		return min;
	}
	
	public static int findMax(int[] x) {
		int max=x[0];
		for(int i=0; i<x.length;i++) {
			if(x[i]>max)
				max=x[i];
		}
		return max;
	}
}
