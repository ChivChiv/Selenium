import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.util.List;

public class TableSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "D:\\Harsha\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/offers");
		driver.findElement(By.cssSelector("tr td:nth-child(2)")).click();
		driver.findElement(By.cssSelector("tr td:nth-child(2)")).click();
		List<WebElement> firstcList=driver.findElements(By.cssSelector("tr td:nth-child(2)"));
		ArrayList<String> originalList=new ArrayList<String>();
		for(int i=0;i<firstcList.size();i++)
		{
			originalList.add(firstcList.get(i).getText());
		}
		ArrayList<String> copylist=new ArrayList<String>();
		
		for(int i=0;i<originalList.size();i++)
		{
			copylist.add(originalList.get(i));
		}
		System.out.println(".............Copied.......................");
		Collections.sort(copylist);
		Collections.reverse(copylist);
		for(String s: copylist)
		{
			System.out.println(s);
		}
		
		System.out.println("..............Original......................");
		
		for(String s: originalList)
		{
			System.out.println(s);
		}
		Assert.assertTrue(originalList.equals(copylist));
	}

}
